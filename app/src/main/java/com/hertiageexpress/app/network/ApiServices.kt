package com.hertiageexpress.app.network

import com.hertiageexpress.app.models.*
import retrofit2.http.*

interface ApiServices {
    @GET("api/gettours")
    suspend fun getTours(@Header("DToken")
                             token: String): Response<ArrayList<Tour>>

    @GET("api/home")
    suspend fun getHome(@Header("DToken")
                            token: String): Response<Home>

    @GET("api/landing")
    suspend fun getLanding(@Header("DToken")
                               token: String): Response<Landing>

    @GET("api/Stories")
    suspend fun getStories(@Header("DToken")
                               token: String): Response<ArrayList<Story>>

    /*@POST("api/getStatment")
    suspend fun getStatement(
        @Body statementBody: StatementBody,
        @Header("DToken") token: String
    ): Response<ArrayList<Statement>>*/
}