package com.hertiageexpress.app.network

import com.hertiageexpress.app.models.Response

open class ResponseHandler {
    fun <T : Any> handleSuccess(code: Int, data: T): Response<T> {
        return Response.success(code, data)
    }

    fun <T : Any> handleException(code: Int, e: Exception): Response<T> {
        return Response.error(code, e.message!!)
    }
}
