package com.hertiageexpress.app.utils

object Constants {
    //Test
    const val API_URL = "http://services.heritageexpress.com/"
    const val SERVER_URL = "http://hexpress.dcx360.com/"

    //live
   /* const val API_URL_TOKEN = "https://BMportal.saif-zone.com/"
    const val API_URL = "https://SMportal.saif-zone.com/"
    const val IMAGE_URL = "https://mportal.saif-zone.com/"
    const val WEB_SERVER_DOWNLOAD_LICENSE_FILE = "https://bmportal.saif-zone.com/LicenseFile.aspx?TokenID="
    //const val WEB_SERVER_DOWNLOAD_LICENSE_FILE = "https://bmportal.saif-zone.com/tradelicense.aspx?LicenseNo="
    //val DOWNLOAD_LICENSE_FILE_1 = "https://mportal.saif-zone.com/ConsumeToken.aspx?TokenID="
    //val DOWNLOAD_LICENSE_FILE_2 = "&ReturnURL=http://mportal.saif-zone.com/TradeLicense.aspx?LicenseNo="
    const val REDIRECT_URL = "https://bmportal.saif-zone.com/OathToken.aspx";*/

    const val TIMEOUT: Long = 20
    const val PROTOCOL_SSL = "SSL"
    const val AUTHTYPE = "USER"
    const val BUILDID = "1"
}