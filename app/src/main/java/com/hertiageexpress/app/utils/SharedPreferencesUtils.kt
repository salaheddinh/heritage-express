package com.hertiageexpress.app.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.Gson

object SharedPreferencesUtils {

    private var preferences: SharedPreferences? = null
    private var instance: SharedPreferencesUtils? = null
    private const val NOT_INITIALIZED_ERROR = "Shared Preferences is not initialized"

    fun getInstance(): SharedPreferencesUtils {
        if (instance == null) {
            throw IllegalStateException(NOT_INITIALIZED_ERROR)
        }
        return instance!!
    }

    fun initializeSharedPrefsService(context: Context) {
        instance = SharedPreferencesUtils
        instance!!.preferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveString(key: String, value: String) {
        val sharedPreferencesEditor = preferences?.edit()
        sharedPreferencesEditor?.putString(key, value)
        sharedPreferencesEditor?.apply()
    }

    fun getStringValue(key: String, defaultValue: String?): String {
        return preferences!!.getString(key, defaultValue!!)!!
    }

    fun saveBoolean(key: String, value: Boolean) {
        val sharedPreferencesEditor = preferences?.edit()
        sharedPreferencesEditor?.putBoolean(key, value)
        sharedPreferencesEditor?.apply()
    }

    fun getBooleanValue(key: String, defaultValue: Boolean?): Boolean {
        return preferences!!.getBoolean(key, defaultValue!!)
    }

    fun isGuest(): Boolean {
        val userToken = getStringValue("token", "")
        return userToken.isEmpty()
    }

    fun saveObject(key: String, value: Any) {
        val sharedPreferencesEditor = preferences?.edit()
        val gson = Gson()
        val json = gson.toJson(value)
        sharedPreferencesEditor?.putString(key, json)
        sharedPreferencesEditor?.apply()
    }

    fun <GenericClass> getObject(key: String, classType: Class<GenericClass>): GenericClass {
        val gson = Gson()
        val json = preferences?.getString(key, "")
        val obj = gson.fromJson(json, classType)
        return obj
    }

    fun emptyAllCache() {
        var pref = preferences?.edit()
        pref?.clear()
        pref?.commit()
    }
}