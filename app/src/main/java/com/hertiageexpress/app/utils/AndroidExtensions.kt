package com.hertiageexpress.app.utils

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.hertiageexpress.app.R

fun Activity.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.showKeyboard(view: View) {
    view.requestFocus()
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInputFromWindow(view.windowToken, 0, 0)
}

fun Activity.showLoading(data: View, loading: View, error: View) {
    data.visibility = View.GONE
    loading.visibility = View.VISIBLE
    error.visibility = View.GONE
}

fun Activity.showError(
    data: View,
    loading: View,
    error: View,
    errorText: String,
    errorHandler: () -> Unit
) {
    data.visibility = View.GONE
    loading.visibility = View.GONE
    error.visibility = View.VISIBLE
    val errorTextView = error.findViewById<TextView>(R.id.tvError)
    val retryTextView = error.findViewById<TextView>(R.id.tvRetry)
    errorTextView.text = errorText
    retryTextView.setOnClickListener { errorHandler.invoke() }
}

fun Activity.showData(data: View, loading: View, error: View) {
    data.visibility = View.VISIBLE
    loading.visibility = View.GONE
    error.visibility = View.GONE
}

fun Fragment.showDialog(
    title: String,
    message: String,
    positiveText: String,
    negativeText: String,
    positiveButton: () -> Unit,
    negativeButton: () -> Unit
) {
    AlertDialog.Builder(activity as Context)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(
            positiveText
        ) { _, _ ->
            positiveButton.invoke()
        }
        .setNegativeButton(negativeText)
        { _, _ -> negativeButton.invoke() }
        .show()
}

fun Activity.showDialog(
    title: String,
    message: String,
    positiveText: String,
    negativeText: String,
    positiveButton: () -> Unit,
    negativeButton: () -> Unit
) {
    AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(
            positiveText
        ) { _, _ ->
            positiveButton.invoke()
        }
        .setNegativeButton(negativeText)
        { _, _ -> negativeButton.invoke() }
        .show()
}

fun Fragment.showToast(msg: String) {
    Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
}

fun Activity.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}

fun Fragment.showEmpty(data: View, loading: View, error: View, empty: View ) {
    data.visibility = View.GONE
    loading.visibility = View.GONE
    error.visibility = View.GONE
    empty.visibility = View.VISIBLE
}

fun Fragment.showLoading(data: View, loading: View, error: View, empty: View ) {
    data.visibility = View.GONE
    loading.visibility = View.VISIBLE
    error.visibility = View.GONE
    empty.visibility = View.GONE
}

fun Fragment.showError(
    data: View,
    loading: View,
    error: View,
    empty: View,
    errorText: String,
    errorHandler: () -> Unit
) {
    data.visibility = View.GONE
    loading.visibility = View.GONE
    empty.visibility = View.GONE
    error.visibility = View.VISIBLE
    val errorTextView = error.findViewById<TextView>(R.id.tvError)
    val retryTextView = error.findViewById<TextView>(R.id.tvRetry)
    errorTextView.text = errorText
    retryTextView.setOnClickListener { errorHandler.invoke() }
}

fun Fragment.showData(data: View, loading: View, error: View, empty: View) {
    data.visibility = View.VISIBLE
    loading.visibility = View.GONE
    error.visibility = View.GONE
    empty.visibility = View.GONE
}

fun Fragment.keyboardChanged(root: View, keyboardListener: (isShow: Boolean) -> Unit) {
    root.viewTreeObserver.addOnGlobalLayoutListener {
        val r = Rect()
        root.getWindowVisibleDisplayFrame(r);
        val screenHeight = root.rootView.height

        val keypadHeight = screenHeight - r.bottom

        if (keypadHeight > screenHeight * 0.15) {
            keyboardListener(true)
        } else {
            keyboardListener(false)
        }
    }
}

fun Activity.keyboardChanged(root: View, keyboardListener: (isShow: Boolean) -> Unit) {
    root.viewTreeObserver.addOnGlobalLayoutListener {
        val r = Rect()
        root.getWindowVisibleDisplayFrame(r);
        val screenHeight = root.rootView.height

        val keypadHeight = screenHeight - r.bottom

        if (keypadHeight > screenHeight * 0.15) {
            keyboardListener(true)
        } else {
            keyboardListener(false)
        }
    }
}

fun Fragment.hideKeyboard(view: View) {
    val inputMethodManager =
        activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.showKeyboard(view: View) {
    view.requestFocus()
    val inputMethodManager =
        activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInputFromWindow(view.windowToken, 0, 0)
}
