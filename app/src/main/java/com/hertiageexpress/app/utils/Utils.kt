package com.hertiageexpress.app.utils

import java.util.*

object Utils {

    fun getWelcomeMessage(): String {
        val calendar = Calendar.getInstance()
        when (calendar.get(Calendar.HOUR_OF_DAY)) {
            in 5..11 -> {
                return "Good Morning"
            }
            in 12..16 -> {
                return "Good Afternoon"
            }
            in 17..20 -> {
                return "Good Evening"
            }
            in 21..24 -> {
                return "Good Night"
            }
            in 0..4 -> {
                return "Good Night"
            }
            else -> {
                return "Good Afternoon"
            }
        }
    }
}