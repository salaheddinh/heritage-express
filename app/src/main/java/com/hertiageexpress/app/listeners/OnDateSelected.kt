package com.hertiageexpress.app.listeners

import java.util.*

public interface OnDateSelected {
    fun onDateselected(date : Date)
}