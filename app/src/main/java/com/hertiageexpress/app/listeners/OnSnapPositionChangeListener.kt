package com.hertiageexpress.app.listeners

interface OnSnapPositionChangeListener {

    fun onSnapPositionChange(position: Int)
}