package com.hertiageexpress.app.pages.tours

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.models.Status
import com.hertiageexpress.app.models.Tour
import com.hertiageexpress.app.pages.tourDetails.TourDetailsActivity
import com.hertiageexpress.app.utils.*
import kotlinx.android.synthetic.main.activity_tours.*
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ToursActivity : AppCompatActivity() {
    private val mViewModel: TourViewModel by viewModel()
    private val resultObserver = Observer<Response<ArrayList<Tour>>> { result ->
        when (result.status) {
            Status.SUCCESS -> {
                showData(rvTours, loading, errorView)
                setData(result.data!!)
            }

            Status.ERROR -> {
                showError(rvTours, loading, errorView, result.message!!) {
                    loadData()
                }
            }

            Status.LOADING -> {
                showLoading(rvTours, loading, errorView)
            }
        }
    }

    private fun loadData() {
        mViewModel.getTours().observe(this, resultObserver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tours)
        init()
        loadData()
    }

    private fun init() {
        collapsing_toolbar_layout.setExpandedTitleColor(resources.getColor(R.color.white))
        collapsing_toolbar_layout.setCollapsedTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        rvTours.layoutManager = LinearLayoutManager(this@ToursActivity)
    }

    private fun setData(items: ArrayList<Tour>) {
        val adapter = ToursAdapter(
            items,
            object :
                ToursAdapter.ToursListener {
                override fun onTourClicked(tour: Tour, position: Int) {
                    val intent = Intent(this@ToursActivity, TourDetailsActivity::class.java)
                    intent.putExtra("tour", tour)
                    startActivity(intent)
                }
            })

        rvTours.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}