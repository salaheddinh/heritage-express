package com.hertiageexpress.app.pages.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Media
import com.hertiageexpress.app.models.Story
import com.hertiageexpress.app.pages.mediaCenter.MediaCenterAdapter

class StoriesAdapter(
    private val data: ArrayList<Story>,
    private val mStoryListener: StoryListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mInflater: LayoutInflater? = null

    private val mOnclickListener = View.OnClickListener { view ->
        if (view.id == R.id.root) {
            val story = view.getTag(R.string.tag_item) as Story
            mStoryListener.onStoryClicked(story, 0)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.context)
        }
        val view = mInflater!!.inflate(R.layout.item_story, parent, false)
        return EventViewHolder(
            view,
            mOnclickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventViewHolder) {
            holder.setData(data[position])
        }
    }

    class EventViewHolder(
        itemView: View,
        listener: View.OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {

        private val root = itemView.findViewById<View>(R.id.root)
        private val image = itemView.findViewById<ImageView>(R.id.imageView1)
        private val title = itemView.findViewById<TextView>(R.id.tvTitle)
        private val author = itemView.findViewById<TextView>(R.id.tvAuthor)

        init {
            root.setOnClickListener(listener)
        }

        fun setData(item: Story) {
            root.setTag(R.string.tag_item, item)

            Glide.with(itemView.context).load(item.imageUrl).into(image)
            title.text = item.title
            author.text = "By ${item.author}"
        }
    }

    interface StoryListener {
        fun onStoryClicked(story: Story, position: Int)
    }
}