package com.hertiageexpress.app.pages.mediaCenter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Media
import com.hertiageexpress.app.pages.postContent.PostContentActivity
import kotlinx.android.synthetic.main.activity_media_center.*

class MediaCenterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media_center)
        init()
    }

    private fun init() {
        collapsing_toolbar_layout.setExpandedTitleColor(resources.getColor(R.color.white))
        collapsing_toolbar_layout.setCollapsedTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        rvMediaCenter.layoutManager = LinearLayoutManager(this@MediaCenterActivity)

        val media = arrayListOf<Media>(

        )
        val adapter = MediaCenterAdapter(
            media,
            object : MediaCenterAdapter.MediaListener {
                override fun onMediaClicked(media: Media, position: Int) {
                    startActivity(Intent(this@MediaCenterActivity, PostContentActivity::class.java))
                }
            })

        rvMediaCenter.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}