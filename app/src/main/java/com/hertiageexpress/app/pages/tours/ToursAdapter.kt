package com.hertiageexpress.app.pages.tours

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Tour
import kotlinx.android.synthetic.main.activity_booking.*

class ToursAdapter(
    private val data: ArrayList<Tour>,
    private val mToursListener: ToursListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mInflater: LayoutInflater? = null

    private val mOnclickListener = View.OnClickListener { view ->
        if (view.id == R.id.root) {
            val tour = view.getTag(R.string.tag_item) as Tour
            val position = view.getTag(R.string.tag_position) as Int
            mToursListener.onTourClicked(tour, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.context)
        }
        val view = mInflater!!.inflate(R.layout.item_tour, parent, false)
        return EventViewHolder(
            view,
            mOnclickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventViewHolder) {
            holder.setData(data[position], position)
        }
    }

    class EventViewHolder(
        itemView: View,
        listener: View.OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        private val root = itemView.findViewById<View>(R.id.root)
        private val image = itemView.findViewById<ImageView>(R.id.imageView1)

        private val title = itemView.findViewById<TextView>(R.id.tvTitle)
        private val price = itemView.findViewById<TextView>(R.id.tvPrice)

        init {
            root.setOnClickListener(listener)
        }

        fun setData(tour: Tour, position: Int) {
            root.setTag(R.string.tag_item, tour)
            root.setTag(R.string.tag_position, position)

            Glide.with(itemView.context).load(tour.imageUrl).into(image)
            title.text = tour.Caption
            price.text = tour.Price
        }
    }

    interface ToursListener {
        fun onTourClicked(tour: Tour, position: Int)
    }
}