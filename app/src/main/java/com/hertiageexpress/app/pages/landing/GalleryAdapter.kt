package com.hertiageexpress.app.pages.landing

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Gallery

class GalleryAdapter(
    private val data: ArrayList<Gallery>,
    private val mGalleryListener: GalleryListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mInflater: LayoutInflater? = null

    private val mOnclickListener = View.OnClickListener { view ->
        if (view.id == R.id.root) {
            val gallery = view.getTag(R.string.tag_item) as Gallery
            val position = view.getTag(R.string.tag_position) as Int
            mGalleryListener.onImageClicked(gallery, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.context)
        }
        val view = mInflater!!.inflate(R.layout.item_gallery, parent, false)
        return EventViewHolder(
            view,
            mOnclickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventViewHolder) {
            holder.setData(data[position])
        }
    }

    class EventViewHolder(
        itemView: View,
        listener: View.OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {

        private val root = itemView.findViewById<View>(R.id.root)
        private val image = itemView.findViewById<ImageView>(R.id.imageView1)

        init {
            root.setOnClickListener(listener)
        }

        fun setData(item: Gallery) {
            root.setTag(R.string.tag_item, item)
            root.setTag(R.string.tag_position, position)

            Glide.with(itemView.context).load(item.imageUrl).into(image)
        }

    }

    interface GalleryListener {
        fun onImageClicked(gallery: Gallery, position: Int)
    }
}