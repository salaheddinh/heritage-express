package com.hertiageexpress.app.pages.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.hertiageexpress.app.models.Home
import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.repositories.HomeRepository
import kotlinx.coroutines.Dispatchers

class HomeViewModel(private val repository: HomeRepository) : ViewModel() {
    fun getTours(): LiveData<Response<Home>> {
        return liveData(Dispatchers.IO) {
            emit(Response.loading(null))
            val response = repository.getHome()
            emit(response)
        }
    }
}