package com.hertiageexpress.app.pages.mediaCenter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Media

class MediaCenterAdapter(
    private val data: ArrayList<Media>,
    private val mMediaListener: MediaListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mInflater: LayoutInflater? = null

    private val mOnclickListener = View.OnClickListener { view ->
        if (view.id == R.id.root) {
            val media = view.getTag(R.string.tag_item) as Media
            val position = view.getTag(R.string.tag_position) as Int
            mMediaListener.onMediaClicked(media, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.context)
        }
        val view = mInflater!!.inflate(R.layout.item_media_list, parent, false)
        return EventViewHolder(
            view,
            mOnclickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventViewHolder) {
            holder.setData(data[position], position)
        }
    }

    class EventViewHolder(
        itemView: View,
        listener: View.OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        private val root = itemView.findViewById<View>(R.id.root)
        private val image = itemView.findViewById<ImageView>(R.id.ivMediaImage)
        private val title = itemView.findViewById<TextView>(R.id.tvTitle)

        init {
            root.setOnClickListener(listener)
        }

        fun setData(media: Media, position: Int) {
            root.setTag(R.string.tag_item, media)
            root.setTag(R.string.tag_position, position)

            Glide.with(itemView.context).load(media.imageUrl).into(image)
            title.text = media.Title
        }
    }

    interface MediaListener {
        fun onMediaClicked(media: Media, position: Int)
    }
}