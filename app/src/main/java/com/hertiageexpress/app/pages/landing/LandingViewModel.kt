package com.hertiageexpress.app.pages.landing

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.hertiageexpress.app.models.Home
import com.hertiageexpress.app.models.Landing
import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.repositories.HomeRepository
import com.hertiageexpress.app.repositories.LandingRepository
import kotlinx.coroutines.Dispatchers

class LandingViewModel(private val repository: LandingRepository) : ViewModel() {
    fun getLanding(): LiveData<Response<Landing>> {
        return liveData(Dispatchers.IO) {
            emit(Response.loading(null))
            val response = repository.getLanding()
            emit(response)
        }
    }
}