package com.hertiageexpress.app.pages.tours

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.models.Tour
import com.hertiageexpress.app.repositories.ToursRepository
import kotlinx.coroutines.Dispatchers

class TourViewModel(private val repository: ToursRepository) : ViewModel() {
    fun getTours(): LiveData<Response<ArrayList<Tour>>> {
        return liveData(Dispatchers.IO) {
            emit(Response.loading(null))
            val response = repository.getTours()
            emit(response)
        }
    }
}