package com.hertiageexpress.app.pages.home

import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.hertiageexpress.app.R
import com.hertiageexpress.app.listeners.OnSnapPositionChangeListener
import com.hertiageexpress.app.listeners.SnapOnScrollListener
import com.hertiageexpress.app.models.*
import com.hertiageexpress.app.pages.postContent.PostContentActivity
import com.hertiageexpress.app.pages.tours.ToursActivity
import com.hertiageexpress.app.utils.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.ivPause
import kotlinx.android.synthetic.main.fragment_home.pageIndicatorView
import kotlinx.android.synthetic.main.fragment_home.pageIndicatorView2
import kotlinx.android.synthetic.main.fragment_home.rvStories
import kotlinx.android.synthetic.main.layout_empty.*
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException

class HomeFragment : Fragment(R.layout.fragment_home) {
    private var isPlaying = false
    private var currentAudio =
        "https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3"
    private var mp: MediaPlayer? = null
    private var audioProgress = 0

    private val mViewModel: HomeViewModel by viewModel()
    private val resultObserver = Observer<Response<Home>> { result ->
        when (result.status) {
            Status.SUCCESS -> {
                showData(data, loading, errorView, emptyView)
                setData(result.data!!)
            }

            Status.ERROR -> {
                showError(data, loading, errorView, emptyView, result.message!!) {
                    loadData()
                }
            }

            Status.LOADING -> {
                showLoading(data, loading, errorView, emptyView)
            }
        }
    }

    private fun loadData() {
        mViewModel.getTours().observe(this, resultObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        loadData()
    }

    private fun setData(home: Home) {
        val adapter =
            StoriesAdapter(home.stories, object : StoriesAdapter.StoryListener {
                override fun onStoryClicked(story: Story, position: Int) {
                    vAudioPlayer.visibility = View.VISIBLE
                    tvStoryTitle.text = story.title
                    tvStoryAuthor.text = story.author
                }

            })
        rvStories.adapter = adapter
        pageIndicatorView.count = home.stories.size

        val adapter2 =
            MediaCenterAdapter(home.mediaCenter, object : MediaCenterAdapter.MediaListener {
                override fun onMediaClicked(media: Media, position: Int) {
                    val intent = Intent(activity, PostContentActivity::class.java)
                    intent.putExtra("media", media)
                    startActivity(intent)
                }
            })
        rvMediaCenter.adapter = adapter2

        pageIndicatorView2.count = home.mediaCenter.size

    }

    private fun init() {
        tvWelcome.text = Utils.getWelcomeMessage()

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvStories.layoutManager = layoutManager

        val helper = LinearSnapHelper()
        helper.attachToRecyclerView(rvStories)

        rvStories.attachSnapHelperWithListener(
            helper,
            SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
            object :
                OnSnapPositionChangeListener {
                override fun onSnapPositionChange(position: Int) {
                    pageIndicatorView.selection = position
                }
            })

        val layoutManager2 = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvMediaCenter.layoutManager = layoutManager2

        val helper2 = LinearSnapHelper()
        helper2.attachToRecyclerView(rvMediaCenter)

        rvMediaCenter.attachSnapHelperWithListener(
            helper2,
            SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
            object :
                OnSnapPositionChangeListener {
                override fun onSnapPositionChange(position: Int) {
                    pageIndicatorView2.selection = position
                }
            })

        ivFacebook.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/heritageexpress"))
            startActivity(browserIntent)
        }

        ivInstagram.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/heritageexpress/"))
            startActivity(browserIntent)
        }

        ivTwitter.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/Heritageexpres"))
            startActivity(browserIntent)
        }

        cvTickets.setOnClickListener {
            startActivity(Intent(activity, ToursActivity::class.java))
        }

        cvTours.setOnClickListener {
            startActivity(Intent(activity, ToursActivity::class.java))
        }

        ivBlackImage.setOnClickListener {
            stopPlaying()
            vAudioPlayer.visibility = View.GONE
        }

        ivPause.setOnClickListener {
            if (currentAudio.isNotEmpty()) {
                if (!isPlaying) {
                    isPlaying = true
                    if (mp == null) {
                        try {
                            mp = MediaPlayer()
                            mp?.setAudioStreamType(AudioManager.STREAM_MUSIC)
                            val url = currentAudio
                            mp?.setDataSource(url)
                            mp?.prepareAsync()
                        } catch (e: IOException) {
                            Log.e("Media Pleayer", "prepare() failed")
                        }
                    } else {
                        mp?.seekTo(audioProgress)
                        mp?.start()
                    }
                    mp?.setOnPreparedListener {
                        mp?.seekTo(audioProgress)
                        mp?.start()
                    }
                    ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_pause))
                    playProgress()
                } else {
                    ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
                    audioProgress = mp!!.currentPosition
                    Log.d("audioProgress", audioProgress.toString())
                    isPlaying = false
                    mp?.pause()
                }
            }
        }

        mp?.setOnCompletionListener {
            ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
        }
    }

    private fun playProgress() {
        val mHandler = Handler()
        activity?.runOnUiThread(object : Runnable {
            override fun run() {
                if (mp != null && progressBar != null) {
                    val mCurrentPosition: Int = mp!!.currentPosition / 1000
                    progressBar.progress = mCurrentPosition
                }
                mHandler.postDelayed(this, 1000)
            }
        })
    }

    private fun stopPlaying() {
        mp?.release()
        if (ivPause != null)
            ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
        isPlaying = false
        mp = null
    }

    override fun onDestroy() {
        super.onDestroy()
        stopPlaying()
    }
}