package com.hertiageexpress.app.pages.stories

import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.hertiageexpress.app.R
import com.hertiageexpress.app.listeners.OnSnapPositionChangeListener
import com.hertiageexpress.app.listeners.SnapOnScrollListener
import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.models.Status
import com.hertiageexpress.app.models.Story
import com.hertiageexpress.app.models.Tour
import com.hertiageexpress.app.pages.home.StoriesAdapter
import com.hertiageexpress.app.utils.attachSnapHelperWithListener
import com.hertiageexpress.app.utils.showData
import com.hertiageexpress.app.utils.showError
import com.hertiageexpress.app.utils.showLoading
import kotlinx.android.synthetic.main.fragment_stories.*
import kotlinx.android.synthetic.main.layout_empty.*
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException


class StoriesFragment : Fragment(R.layout.fragment_stories) {
    private var isPlaying = false
    private var currentAudio =
        "https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3"
    private var mp: MediaPlayer? = null
    private var audioProgress = 0

    private val mViewModel: StoriesViewModel by viewModel()
    private val resultObserver = Observer<Response<ArrayList<Story>>> { result ->
        when (result.status) {
            Status.SUCCESS -> {
                showData(data, loading, errorView, emptyView)
                setData(result.data!!)
            }

            Status.ERROR -> {
                showError(data, loading, errorView, emptyView, result.message!!) {
                    loadData()
                }
            }

            Status.LOADING -> {
                showLoading(data, loading, errorView, emptyView)
            }
        }
    }

    private fun loadData() {
        mViewModel.getStories().observe(this, resultObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        loadData()
    }

    private fun setData(items: ArrayList<Story>) {
        val adapter =
            StoriesAdapter(items, object : StoriesAdapter.StoryListener {
                override fun onStoryClicked(story: Story, position: Int) {
                    tvTitle.text = story.title
                    tvAuthor.text = story.author
                    stopPlaying()
                    ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
                    audioProgress = 0
                    isPlaying = false
                    progressBar.progress = 0
                    mp?.pause()
                }
            })
        rvStories.adapter = adapter
        pageIndicatorView.count = items.size
    }

    private fun init() {
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvStories.layoutManager = layoutManager


        val helper = LinearSnapHelper()
        helper.attachToRecyclerView(rvStories)

        rvStories.attachSnapHelperWithListener(
            helper,
            SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
            object :
                OnSnapPositionChangeListener {
                override fun onSnapPositionChange(position: Int) {
                    pageIndicatorView.selection = position
                }
            })

        /*ivPause.setOnClickListener {
            if (ivPause.tag == false) {
                ivPause.tag = true
                ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
                mCountDownTimer?.pause()
            } else {
                ivPause.tag = false
                ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_pause))
                if (isFinished){
                    mCountDownTimer?.restart()
                }else {
                    mCountDownTimer?.start()
                }
            }
        }*/

        ivPause.setOnClickListener {
            if (currentAudio.isNotEmpty()) {
                if (!isPlaying) {
                    isPlaying = true
                    if (mp == null) {
                        try {
                            mp = MediaPlayer()
                            mp?.setAudioStreamType(AudioManager.STREAM_MUSIC)
                            val url = currentAudio
                            mp?.setDataSource(url)
                            mp?.prepareAsync()
                        } catch (e: IOException) {
                            Log.e("Media Pleayer", "prepare() failed")
                        }
                    } else {
                        mp?.seekTo(audioProgress)
                        mp?.start()
                    }
                    mp?.setOnPreparedListener {
                        mp?.seekTo(audioProgress)
                        mp?.start()
                    }
                    ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_pause))
                    playProgress()
                } else {
                    ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
                    audioProgress = mp!!.currentPosition
                    Log.d("audioProgress", audioProgress.toString())
                    isPlaying = false
                    mp?.pause()
                }
            }
        }

        mp?.setOnCompletionListener {
            ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
        }
        /*progressBar.progress = i
        mCountDownTimer = object : CountDownTimerExt(5000, 100) {
            override fun onTimerTick(millisUntilFinished: Long) {
                Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                i++
                progressBar.progress = (i * 100 / (5000 / 100))
            }

            override fun onTimerFinish() {
                ivPause.tag = true
                ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
                i++
                progressBar.progress = 100
                isFinished = true
            }
        }*/
    }

    private fun playProgress() {
        val mHandler = Handler()
        activity?.runOnUiThread(object : Runnable {
            override fun run() {
                if (mp != null && progressBar != null) {
                    val mCurrentPosition: Int = mp!!.currentPosition / 1000
                    progressBar.progress = mCurrentPosition
                }
                mHandler.postDelayed(this, 1000)
            }
        })
    }

    private fun stopPlaying() {
        mp?.release()
        mp = null
    }

    override fun onDestroy() {
        super.onDestroy()
        stopPlaying()
    }

    private fun resetMediaPlayer() {
        stopPlaying()
        audioProgress = 0
        isPlaying = false
        ivPause.setImageDrawable(resources.getDrawable(R.drawable.ic_play_circle))
    }
}