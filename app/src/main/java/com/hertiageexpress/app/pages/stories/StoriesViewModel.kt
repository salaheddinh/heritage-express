package com.hertiageexpress.app.pages.stories

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.models.Story
import com.hertiageexpress.app.models.Tour
import com.hertiageexpress.app.repositories.StoriesRepository
import com.hertiageexpress.app.repositories.ToursRepository
import kotlinx.coroutines.Dispatchers

class StoriesViewModel(private val repository: StoriesRepository) : ViewModel() {
    fun getStories(): LiveData<Response<ArrayList<Story>>> {
        return liveData(Dispatchers.IO) {
            emit(Response.loading(null))
            val response = repository.getStories()
            emit(response)
        }
    }
}