package com.hertiageexpress.app.pages.game

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.extensions.HdrImageCaptureExtender
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import com.hertiageexpress.app.R
import com.hertiageexpress.app.pages.MainActivity
import com.hertiageexpress.app.pages.UnityPlayerActivity
import kotlinx.android.synthetic.main.fragment_game.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class GameFragment :  Fragment(R.layout.fragment_game) {
    private val executor: Executor = Executors.newSingleThreadExecutor()
    private val REQUEST_CODE_PERMISSIONS = 1001
    private val REQUIRED_PERMISSIONS = arrayOf(
        "android.permission.CAMERA",
        "android.permission.WRITE_EXTERNAL_STORAGE","android.permission.READ_EXTERNAL_STORAGE","android.permission.WAKE_LOCK"
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                activity!!,
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }
        click.setOnClickListener {
            activity?.let {
                val intent = Intent(it, UnityPlayerActivity::class.java)
                intent.putExtra("code","${ticket.text}")
                it.startActivity(intent)
            }
        }
    }

    private fun startCamera() {
        val cameraProviderFuture: ListenableFuture<ProcessCameraProvider> =
            ProcessCameraProvider.getInstance(activity as Context)
        cameraProviderFuture.addListener(Runnable {
            try {
                val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
                bindPreview(cameraProvider)
            } catch (e: ExecutionException) {
                val s = ""
                // No errors need to be handled for this Future.
                // This should never be reached.
            } catch (e: InterruptedException) {
                val s = ""
            }
        }, ContextCompat.getMainExecutor(activity))
    }

    fun bindPreview(@NonNull cameraProvider: ProcessCameraProvider) {
        val preview = Preview.Builder()
            .build()
        val cameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()
        val imageAnalysis = ImageAnalysis.Builder()
            .build()
        val builder = ImageCapture.Builder()

        //Vendor-Extensions (The CameraX extensions dependency in build.gradle)
        val hdrImageCaptureExtender =
            HdrImageCaptureExtender.create(builder)

        // Query if extension is available (optional).
        if (hdrImageCaptureExtender.isExtensionAvailable(cameraSelector)) {
            // Enable the extension if available.
            hdrImageCaptureExtender.enableExtension(cameraSelector)
        }
        val imageCapture = builder
            .setTargetRotation(activity!!.windowManager.defaultDisplay.rotation)
            .build()
        preview.setSurfaceProvider(previewView!!.surfaceProvider)
        val camera: Camera = cameraProvider.bindToLifecycle(
            viewLifecycleOwner,
            cameraSelector,
            preview,
            imageAnalysis,
            imageCapture
        )

//        captureImg.setOnClickListener {
//            val mDateFormat = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
//            val file =
//                File(getBatchDirectoryName(), mDateFormat.format(Date()).toString() + ".jpg")
//            val outputFileOptions: ImageCapture.OutputFileOptions =
//                ImageCapture.OutputFileOptions.Builder(file).build()
//            imageCapture.takePicture(
//                outputFileOptions,
//                executor,
//                object : ImageCapture.OnImageSavedCallback {
//                    override fun onImageSaved(@NonNull outputFileResults: ImageCapture.OutputFileResults) {
//                        Handler().post(Runnable {
//                            Toast.makeText(
//                                activity,
//                                "Image Saved successfully",
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        })
//                    }
//
//                    override fun onError(@NonNull error: ImageCaptureException) {
//                        error.printStackTrace()
//                    }
//                })
//        }
    }
fun click(v:View){
    activity?.let{
        val intent = Intent (it, UnityPlayerActivity::class.java)
        it.startActivity(intent)
    }
}
    fun getBatchDirectoryName(): String? {
        var app_folder_path = ""
        app_folder_path =
            Environment.getExternalStorageDirectory().toString().toString() + "/images"
        val dir = File(app_folder_path)
        if (!dir.exists() && !dir.mkdirs()) {
        }
        return app_folder_path
    }

    private fun allPermissionsGranted(): Boolean {
        for (permission in REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(
                    activity as Context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(activity, "Permissions not granted by the user.", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}