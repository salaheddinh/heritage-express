package com.hertiageexpress.app.pages.booking

import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.listeners.OnDateSelected
import com.hertiageexpress.app.models.Tour
import com.hertiageexpress.app.pages.custom.DatePickerFragment
import kotlinx.android.synthetic.main.activity_booking.*
import kotlinx.android.synthetic.main.activity_booking.collapsing_toolbar_layout
import kotlinx.android.synthetic.main.activity_booking.expandedImage
import kotlinx.android.synthetic.main.activity_booking.toolbar
import kotlinx.android.synthetic.main.activity_tours.*
import java.text.SimpleDateFormat
import java.util.*

class BookingActivity : AppCompatActivity(), View.OnClickListener {
    private val tour by lazy { intent.getSerializableExtra("tour") as Tour }

    var adults = MutableLiveData<Int>()
    var children = MutableLiveData<Int>()
    var infants = MutableLiveData<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking)
        init()
        setData()
        setObservers()
    }

    private fun setData() {
        collapsing_toolbar_layout.title = tour.Caption
        Glide.with(this).load(tour.imageUrl).into(expandedImage)
    }

    private fun setObservers() {
        adults.observe(this, Observer {
            tvAdults.text = adults.value.toString()
        })
        children.observe(this, Observer {
            tvChildren.text = children.value.toString()
        })
        infants.observe(this, Observer {
            tvInfants.text = infants.value.toString()
        })
    }

    private fun init() {
        collapsing_toolbar_layout.setExpandedTitleColor(resources.getColor(R.color.white))
        collapsing_toolbar_layout.setCollapsedTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        adults.value = 2
        children.value = 0
        infants.value = 0
        ivAddAdult.setOnClickListener(this)
        ivAddChild.setOnClickListener(this)
        ivAddInfant.setOnClickListener(this)
        ivRemoveAdult.setOnClickListener(this)
        ivRemoveChild.setOnClickListener(this)
        ivRemoveInfant.setOnClickListener(this)
        etDate.setOnClickListener {
            val newFragment =
                DatePickerFragment()
            newFragment.onDateSelected = object :
                OnDateSelected {
                override fun onDateselected(date: Date) {
                    val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                    val fullDate = sdf.format(date.time)
                    etDate.setText(fullDate)
                }
            }
            newFragment.show(supportFragmentManager, "datePicker")
        }

        tvTime1.setOnClickListener {
            tvTime1.backgroundTintList = ColorStateList.valueOf(
                resources.getColor(
                    R.color.colorPrimary
                )
            )
            tvTime2.backgroundTintList = ColorStateList.valueOf(
                resources.getColor(
                    R.color.grey
                )
            )
        }

        tvTime2.setOnClickListener {
            tvTime1.backgroundTintList = ColorStateList.valueOf(
                resources.getColor(
                    R.color.grey
                )
            )
            tvTime2.backgroundTintList = ColorStateList.valueOf(
                resources.getColor(
                    R.color.colorPrimary
                )
            )
        }

        tvFinishBooking.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivAddAdult -> {
                val newValue = adults.value!! + 1
                adults.postValue(newValue)
            }
            R.id.ivAddChild -> {
                val newValue = children.value!! + 1
                children.postValue(newValue)
            }
            R.id.ivAddInfant -> {
                val newValue = infants.value!! + 1
                infants.postValue(newValue)
            }
            R.id.ivRemoveAdult -> {
                if (adults.value!! > 0) {
                    val newValue = adults.value!! - 1
                    adults.postValue(newValue)
                }
            }
            R.id.ivRemoveChild -> {
                if (children.value!! > 0) {
                    val newValue = children.value!! - 1
                    children.postValue(newValue)
                }
            }
            R.id.ivRemoveInfant -> {
                if (infants.value!! > 0) {
                    val newValue = infants.value!! - 1
                    infants.postValue(newValue)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}