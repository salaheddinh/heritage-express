package com.hertiageexpress.app.pages.tourDetails

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Tour
import com.hertiageexpress.app.pages.booking.BookingActivity
import kotlinx.android.synthetic.main.activity_booking.*
import kotlinx.android.synthetic.main.activity_tour_details.*
import kotlinx.android.synthetic.main.activity_tour_details.collapsing_toolbar_layout
import kotlinx.android.synthetic.main.activity_tour_details.expandedImage
import kotlinx.android.synthetic.main.activity_tour_details.toolbar

class TourDetailsActivity : AppCompatActivity() {
    private val tour by lazy { intent.getSerializableExtra("tour") as Tour }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tour_details)
        init()
        setData()
    }

    private fun setData() {
        collapsing_toolbar_layout.title = tour.Caption
        Glide.with(this).load(tour.imageUrl).into(expandedImage)
        tvPrice.text = tour.Price
        tvDescription.text = tour.Description
    }

    private fun init() {
        collapsing_toolbar_layout.setExpandedTitleColor(resources.getColor(R.color.white))
        collapsing_toolbar_layout.setCollapsedTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        tvBookNow.setOnClickListener {
            val intent = Intent(this@TourDetailsActivity, BookingActivity::class.java)
            intent.putExtra("tour", tour)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}