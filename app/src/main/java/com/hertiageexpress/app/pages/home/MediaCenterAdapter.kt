package com.hertiageexpress.app.pages.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Media

class MediaCenterAdapter(
    private val data: ArrayList<Media>,
    private val mMediaListener: MediaListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mInflater: LayoutInflater? = null

    private val mOnclickListener = View.OnClickListener { view ->
        if (view.id == R.id.root) {
            val media = view.getTag(R.string.tag_item) as Media
            val position = view.getTag(R.string.tag_position) as Int
            mMediaListener.onMediaClicked(media, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.context)
        }
        val view = mInflater!!.inflate(R.layout.item_media, parent, false)
        return EventViewHolder(
            view,
            mOnclickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventViewHolder) {
            holder.setData(data[position])
        }
    }

    class EventViewHolder(
        itemView: View,
        listener: View.OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {

        private val root = itemView.findViewById<View>(R.id.root)
        private val image = itemView.findViewById<ImageView>(R.id.imageView1)
        private val title = itemView.findViewById<TextView>(R.id.tvTitle)

        init {
            root.setOnClickListener(listener)
        }

        fun setData(item: Media) {
            root.setTag(R.string.tag_item, item)
            root.setTag(R.string.tag_position, position)

            Glide.with(itemView.context).load(item.imageUrl).into(image)
            title.text = item.Title
        }
    }

    interface MediaListener {
        fun onMediaClicked(media: Media, position: Int)
    }
}