package com.hertiageexpress.app.pages.landing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.bumptech.glide.Glide
import com.hertiageexpress.app.R
import com.hertiageexpress.app.pages.tours.ToursActivity
import com.hertiageexpress.app.listeners.OnSnapPositionChangeListener
import com.hertiageexpress.app.listeners.SnapOnScrollListener
import com.hertiageexpress.app.models.*
import com.hertiageexpress.app.utils.attachSnapHelperWithListener
import com.hertiageexpress.app.utils.showData
import com.hertiageexpress.app.utils.showError
import com.hertiageexpress.app.utils.showLoading
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerCallback
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import kotlinx.android.synthetic.main.fragment_landing.*
import kotlinx.android.synthetic.main.layout_empty.*
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LandingFragment : Fragment(R.layout.fragment_landing) {
    private val mViewModel: LandingViewModel by viewModel()
    private val resultObserver = Observer<Response<Landing>> { result ->
        when (result.status) {
            Status.SUCCESS -> {
                showData(data, loading, errorView, emptyView)
                setData(result.data!!)
            }

            Status.ERROR -> {
                showError(data, loading, errorView, emptyView, result.message!!) {
                    loadData()
                }
            }

            Status.LOADING -> {
                showLoading(data, loading, errorView, emptyView)
            }
        }
    }

    private fun loadData() {
        mViewModel.getLanding().observe(this, resultObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        initVideoPlayer()
        loadData()
    }

    private fun setData(landing: Landing) {
        val adapter =
            GalleryAdapter(landing.gallery, object : GalleryAdapter.GalleryListener {
                override fun onImageClicked(gallery: Gallery, position: Int) {
                    vImageGallery.visibility = View.VISIBLE
                    Glide.with(activity as Context).load(gallery.imageUrl).into(ivImage)
                }
            })

        rvGallery.adapter = adapter
        pageIndicatorView.count = landing.gallery.size

        val adapter2 =
            StoryTellersAdapter(landing.storyTellers, object :
                StoryTellersAdapter.StoryTellerListener {
                override fun onStoryTellerClicked(storyTellers: StoryTellers, position: Int) {
                    vVideo.visibility = View.VISIBLE
                    youTubePlayerView.getYouTubePlayerWhenReady(object : YouTubePlayerCallback {
                        override fun onYouTubePlayer(youTubePlayer: YouTubePlayer) {
                            val videoId = storyTellers.VideoLink.substring(storyTellers.VideoLink.indexOf("=") + 1, storyTellers.VideoLink.length)
                            youTubePlayer.loadVideo(videoId, 0f)
                        }
                    })
                }
            })

        rvTellers.adapter = adapter2
        pageIndicatorView2.count = landing.storyTellers.size

    }

    private fun init() {
        ivBlackImage.setOnClickListener {
            vImageGallery.visibility = View.GONE
        }

        ivBlackImageVideo.setOnClickListener {
            vVideo.visibility = View.GONE
            youTubePlayerView.getYouTubePlayerWhenReady(object : YouTubePlayerCallback {
                override fun onYouTubePlayer(youTubePlayer: YouTubePlayer) {
                    youTubePlayer.pause()
                }
            })
        }

        cvTrips.setOnClickListener {
            startActivity(Intent(activity, ToursActivity::class.java))
        }

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvGallery.layoutManager = layoutManager


        val helper = LinearSnapHelper()
        helper.attachToRecyclerView(rvGallery)

        rvGallery.attachSnapHelperWithListener(
            helper,
            SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
            object :
                OnSnapPositionChangeListener {
                override fun onSnapPositionChange(position: Int) {
                    pageIndicatorView.selection = position
                }
            })

        val layoutManager2 = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvTellers.layoutManager = layoutManager2

        val helper2 = LinearSnapHelper()
        helper2.attachToRecyclerView(rvTellers)

        rvTellers.attachSnapHelperWithListener(
            helper2,
            SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
            object :
                OnSnapPositionChangeListener {
                override fun onSnapPositionChange(position: Int) {
                    pageIndicatorView2.selection = position
                }
            })
    }

    private fun initVideoPlayer() {
        //lifecycle.addObserver(youTubePlayerView)
    }
}