package com.hertiageexpress.app.pages.splash

import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import com.hertiageexpress.app.R
import com.hertiageexpress.app.pages.MainActivity
import com.hertiageexpress.app.pages.login.LoginActivity
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        init()
    }

    private fun init() {
        tvContinueSplash.setOnClickListener {
            goToLogin()
        }

        tvFacebook.setOnClickListener {
            goToComplete()
        }

        tvGoogle.setOnClickListener {
            goToComplete()
        }

        tvWhatsapp.setOnClickListener {
            goToComplete()
        }

        tvGuest.setOnClickListener {
            goToApp()
        }

        tvContinue.setOnClickListener {
            goToApp()
        }
    }

    private fun goToLogin() {
        val bitmap = Blurry.with(this)
            .radius(10)
            .sampling(8)
            .capture(findViewById(R.id.ivBackground)).get()
        ivBackground.setImageDrawable(BitmapDrawable(resources, bitmap))
        vSplash.visibility = View.GONE
        vLogin.alpha = 0f;
        vLogin.visibility = View.VISIBLE;
        vLogin.animate()
            .alpha(1.0f)
            .setDuration(400)
            .setListener(null)
    }

    private fun goToComplete() {
        vLogin.visibility = View.GONE
        vCompleteRegistration.alpha = 0f
        vCompleteRegistration.visibility = View.VISIBLE
        vCompleteRegistration.animate()
            .alpha(1.0f)
            .setDuration(400)
            .setListener(null)
    }

    private fun goToApp() {
        val i = Intent(this, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }
}