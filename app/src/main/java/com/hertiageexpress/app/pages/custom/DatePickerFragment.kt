package com.hertiageexpress.app.pages.custom

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.widget.DatePicker
import com.hertiageexpress.app.listeners.OnDateSelected
import java.util.*

class DatePickerFragment() : DialogFragment(),
    DatePickerDialog.OnDateSetListener {

    var onDateSelected: OnDateSelected? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dialog = DatePickerDialog(activity as Context, this, year, month, day)
        dialog.datePicker.minDate = c.timeInMillis
        return dialog
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        val date = Calendar.getInstance()
        date.set(Calendar.DAY_OF_MONTH, day)
        date.set(Calendar.MONTH, month)
        date.set(Calendar.YEAR, year)
        onDateSelected?.onDateselected(date.time)
    }
}