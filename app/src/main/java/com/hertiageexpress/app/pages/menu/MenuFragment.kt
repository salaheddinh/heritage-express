package com.hertiageexpress.app.pages.menu

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.hertiageexpress.app.R
import com.hertiageexpress.app.pages.mediaCenter.MediaCenterActivity
import com.hertiageexpress.app.pages.notifications.NotificationsActivity
import com.hertiageexpress.app.pages.socialChannels.SocialChannelsActivity
import kotlinx.android.synthetic.main.fragment_menu.*

class MenuFragment : Fragment(R.layout.fragment_menu) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        vMediaCenter.setOnClickListener {
            startActivity(Intent(activity, MediaCenterActivity::class.java))
        }

        vNotifications.setOnClickListener {
            startActivity(Intent(activity, NotificationsActivity::class.java))
        }

        vSocial.setOnClickListener {
            startActivity(Intent(activity, SocialChannelsActivity::class.java))
        }
    }
}