package com.hertiageexpress.app.pages.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.hertiageexpress.app.R
import com.hertiageexpress.app.pages.MainActivity
import com.hertiageexpress.app.pages.completeRegister.CompleteRegisterActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init() {
        tvGuest.setOnClickListener {
            startActivity(Intent(this, CompleteRegisterActivity::class.java))
        }
    }
}