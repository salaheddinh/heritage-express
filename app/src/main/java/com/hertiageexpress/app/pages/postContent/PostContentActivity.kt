package com.hertiageexpress.app.pages.postContent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Media
import com.hertiageexpress.app.models.Tour
import kotlinx.android.synthetic.main.activity_media_center.*
import kotlinx.android.synthetic.main.activity_media_center.toolbar
import kotlinx.android.synthetic.main.activity_post_content.*

class PostContentActivity : AppCompatActivity() {
    private val media by lazy { intent.getSerializableExtra("media") as Media }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_content)
        init()
        setData()
    }

    private fun setData() {
        tvTitle.text = media.Title
        tvDescription.text = Html.fromHtml(media.HTMLEditor)
    }

    private fun init() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}