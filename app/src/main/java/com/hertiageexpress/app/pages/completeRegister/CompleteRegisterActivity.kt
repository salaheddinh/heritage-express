package com.hertiageexpress.app.pages.completeRegister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.hertiageexpress.app.R
import com.hertiageexpress.app.pages.MainActivity
import kotlinx.android.synthetic.main.activity_complete_register.*

class CompleteRegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_register)
        init()
    }

    private fun init() {
        tvContinue.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}