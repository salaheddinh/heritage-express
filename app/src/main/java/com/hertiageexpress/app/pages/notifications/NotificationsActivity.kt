package com.hertiageexpress.app.pages.notifications

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.hertiageexpress.app.R
import com.hertiageexpress.app.models.Media
import com.hertiageexpress.app.pages.mediaCenter.MediaCenterAdapter
import com.hertiageexpress.app.pages.postContent.PostContentActivity
import kotlinx.android.synthetic.main.activity_notifications.*

class NotificationsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
        init()
    }

    private fun init() {
        collapsing_toolbar_layout.setExpandedTitleColor(resources.getColor(R.color.white))
        collapsing_toolbar_layout.setCollapsedTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        rvNotifications.layoutManager = LinearLayoutManager(this@NotificationsActivity)

        val media = arrayListOf<Media>(

        )
        val adapter = NotificationsAdapter(
            media,
            object : NotificationsAdapter.MediaListener {
                override fun onMediaClicked(media: Media, position: Int) {
                    val intent = Intent(this@NotificationsActivity, PostContentActivity::class.java)
                    intent.putExtra("media", media)
                    startActivity(intent)
                }
            })

        rvNotifications.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}