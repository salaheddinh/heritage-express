package com.hertiageexpress.app.repositories

import com.hertiageexpress.app.models.Landing
import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.network.ApiServices
import com.hertiageexpress.app.network.ResponseHandler
import com.hertiageexpress.app.utils.SharedPreferencesUtils

class LandingRepository(
    private val apiService: ApiServices,
    private val responseHandler: ResponseHandler
) {

    suspend fun getLanding(): Response<Landing> {
        try {
            val token =
                SharedPreferencesUtils.getStringValue("token", "5RGT17D11Q637401582173312721B2I1")
            val response = apiService.getLanding(token)

            return if (response.data != null) {
                try {
                    if (response.code == 0)
                        responseHandler.handleSuccess(response.code, response.data)
                    else
                        responseHandler.handleException(response.code, Exception(response.message))

                } catch (e: Exception) {
                    responseHandler.handleException<Landing>(response.code, e)
                }

            } else {
                responseHandler.handleException<Landing>(
                    response.code,
                    Exception("Data not found")
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return responseHandler.handleException<Landing>(
                -1000,
                Exception("General Error")
            )
        }
    }
}