package com.hertiageexpress.app.repositories

import com.hertiageexpress.app.models.Response
import com.hertiageexpress.app.models.Story
import com.hertiageexpress.app.models.Tour
import com.hertiageexpress.app.network.ApiServices
import com.hertiageexpress.app.network.ResponseHandler
import com.hertiageexpress.app.utils.SharedPreferencesUtils

class StoriesRepository(
    private val apiService: ApiServices,
    private val responseHandler: ResponseHandler
) {

    suspend fun getStories(): Response<ArrayList<Story>> {
        try {
            val token = SharedPreferencesUtils.getStringValue("token", "5RGT17D11Q637401582173312721B2I1")
            val response = apiService.getStories(token)

            return if (response.data != null) {
                try {
                    if (response.code == 0)
                        responseHandler.handleSuccess(response.code, response.data)
                    else
                        responseHandler.handleException(response.code, Exception(response.message))

                } catch (e: Exception) {
                    responseHandler.handleException<ArrayList<Story>>(response.code, e)
                }

            } else {
                responseHandler.handleException<ArrayList<Story>>(
                    response.code,
                    Exception("Data not found")
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return responseHandler.handleException<ArrayList<Story>>(
                -1000,
                Exception("General Error")
            )
        }
    }
}