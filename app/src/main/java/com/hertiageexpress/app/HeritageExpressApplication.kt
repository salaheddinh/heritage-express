package com.hertiageexpress.app

import android.app.Application
import com.hertiageexpress.app.di.NetworkModule
import com.hertiageexpress.app.di.RepositoryModule
import com.hertiageexpress.app.di.ViewModelModule
import com.hertiageexpress.app.utils.SharedPreferencesUtils
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class HeritageExpressApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@HeritageExpressApplication)

            printLogger(level = Level.DEBUG)
            modules(
                listOf(
                    NetworkModule,
                    ViewModelModule,
                    RepositoryModule
                )
            )
        }

        SharedPreferencesUtils.initializeSharedPrefsService(this@HeritageExpressApplication)
    }
}