package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName
import com.hertiageexpress.app.utils.Constants
import java.io.Serializable

class Story(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("author")
    val author: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("audio")
    val audio: String
) : Serializable {
    val imageUrl: String
        get() {
            return Constants.SERVER_URL + "download.aspx?fileID=$image&width=500"
        }
}