package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName

data class Response<out T>(
    @SerializedName("ErrorCode")
    var code: Int,
    @SerializedName("Data")
    val data: T? = null,
    @SerializedName("Message")
    var message: String? = "Message is Null",
    var status: Status
) {
    companion object {

        fun <T> success(code: Int, data: T?): Response<T> {
            return Response(
                code,
                data,
                null,
                Status.SUCCESS
            )
        }

        fun <T> error(code: Int, msg: String = ""): Response<T> {
            return Response(
                code,
                null,
                msg,
                Status.ERROR
            )
        }

        fun <T> loading(data: T? = null): Response<T> {
            return Response(
                0,
                data,
                "",
                Status.LOADING
            )
        }
    }
}
