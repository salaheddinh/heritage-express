package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName
class Landing(
    @SerializedName("gallery")
    val gallery: ArrayList<Gallery>,
    @SerializedName("storyTellers")
    val storyTellers: ArrayList<StoryTellers>
)