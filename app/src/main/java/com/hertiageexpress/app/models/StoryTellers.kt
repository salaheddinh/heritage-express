package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName
import com.hertiageexpress.app.utils.Constants
import java.io.Serializable

class StoryTellers(
    @SerializedName("Id")
    val Id: Int,
    @SerializedName("FName")
    val FName: String,
    @SerializedName("LName")
    val LName: String,
    @SerializedName("YoutubeLink")
    val VideoLink: String,
    @SerializedName("Picture")
    val Picture: String
) : Serializable {
    val imageUrl: String
        get() {
            return Constants.SERVER_URL + "download.aspx?fileID=$Picture&width=500"
        }
}