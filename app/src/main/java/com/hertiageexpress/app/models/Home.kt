package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName
class Home(
    @SerializedName("stories")
    val stories: ArrayList<Story>,
    @SerializedName("mediaCenter")
    val mediaCenter: ArrayList<Media>
)