package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName
import com.hertiageexpress.app.utils.Constants
import java.io.Serializable

class Tour(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("Caption")
    val Caption: String,
    @SerializedName("Image")
    val Image: String,
    @SerializedName("Price")
    val Price: String,
    @SerializedName("Description")
    val Description: String
) : Serializable {
    val imageUrl: String
        get() {
            return Constants.SERVER_URL + "download.aspx?fileID=$Image&width=500"
        }
}