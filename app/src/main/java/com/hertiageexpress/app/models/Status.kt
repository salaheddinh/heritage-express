package com.hertiageexpress.app.models

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}