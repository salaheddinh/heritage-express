package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName
import com.hertiageexpress.app.utils.Constants
import java.io.Serializable

class Media(
    @SerializedName("Id")
    val Id: Int,
    @SerializedName("Title")
    val Title: String,
    @SerializedName("Description")
    val Description: String,
    @SerializedName("Picture")
    val Picture: String,
    @SerializedName("HTMLEditor")
    val HTMLEditor: String
) : Serializable {
    val imageUrl: String
        get() {
            return Constants.SERVER_URL + "download.aspx?fileID=$Picture&width=500"
        }
}