package com.hertiageexpress.app.models

import com.google.gson.annotations.SerializedName
import com.hertiageexpress.app.utils.Constants
import java.io.Serializable

class Gallery(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("PlanGroup")
    val PlanGroup: String,
    @SerializedName("Image")
    val Image: String
) : Serializable {
    val imageUrl: String
        get() {
            return Constants.SERVER_URL + "download.aspx?fileID=$Image&width=500"
        }
}