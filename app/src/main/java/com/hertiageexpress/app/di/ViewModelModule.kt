package com.hertiageexpress.app.di

import com.hertiageexpress.app.pages.home.HomeViewModel
import com.hertiageexpress.app.pages.landing.LandingViewModel
import com.hertiageexpress.app.pages.stories.StoriesViewModel
import com.hertiageexpress.app.pages.tours.TourViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ViewModelModule = module {
    viewModel { TourViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { StoriesViewModel(get()) }
    viewModel { LandingViewModel(get()) }
}