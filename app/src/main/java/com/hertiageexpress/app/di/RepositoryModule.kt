package com.hertiageexpress.app.di

import com.hertiageexpress.app.repositories.*
import org.koin.dsl.module

val RepositoryModule = module {
    single { ToursRepository(get(), get()) }
    single { HomeRepository(get(), get()) }
    single { StoriesRepository(get(), get()) }
    single { LandingRepository(get(), get()) }
}